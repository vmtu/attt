var express = require('express');
var router = express.Router();
var postgres = require('../libraries/postgres');
var db = require('../config/db');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/login', function(req, res, next) {
    res.render('login', { title: 'Login' });
});

router.post('/login', function(req, res) {
    var username = req.body.username;
    var password = req.body.password;
    var test = /^(?=.{8,20}$)(?![_])(?!.
    *[_]{2})[a-zA-Z0-9_]+(?![_])$/.test(username);
    var upperUsername = username.toUpperCase();
    var upperPassword = password.toUpperCase();
    if ((upperUsername.includes('SELECT') || upperUsername.includes('INSERT') || upperUsername.includes('DROP') || upperUsername.includes('DELETE') || upperUsername.includes('--')) ||
    (upperPassword.includes('SELECT') || upperPassword.includes('INSERT') || upperPassword.includes('DROP') || upperPassword.includes('DELETE') || upperPassword.includes('--'))){
        res.status(200).json({
            status: 'Fail',
            message: 'Username hoặc Password chứa từ khóa không hợp lệ'
        });
    }
    if(test == false) {
        res.status(200).json({
            status: 'Fail',
            message: 'Username chứa ký tự không hợp lệ'
        });
    }else {
        postgres.loginAdmin(username, password).then(function (result) {
            if(result[0] && result[0].username && result[0].password) {
                res.status(200).json({
                    status: 'Success',
                    message: 'Login success',
                    result: result
                });
            }else if (result[0]){
                res.status(200).json({
                    status: 'Fail',
                    message: 'Username or Password incorrect',
                    result: result
                });
            }else {
                res.status(200).json({
                    status: 'Fail',
                    message: 'Username or Password incorrect'
                });
            }
        });
    }




});

module.exports = router;

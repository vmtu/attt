var Sequelize = require('sequelize');
var config = {
    db: {
        pgsql:{
            dialect :'postgres',
            port    : 5432,
            host    :'localhost',
            user    :'postgres',
            pass    :'postgres',
            name    :'attt',
            schema  :'public'
        }
    }
};
var db = {};
const sequelize = new Sequelize(config.db.pgsql.name, config.db.pgsql.user, config.db.pgsql.pass, {
    host: config.db.pgsql.host,
    dialect: config.db.pgsql.dialect, // or 'sqlite', 'postgres', 'mariadb'
    port: config.db.pgsql.port, // or 5432 (for postgres)
    timezone: '+07:00',
    logging: false,
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    }
});

db.sequelize = sequelize;

module.exports = db;